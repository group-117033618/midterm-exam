int tamp = 0;
int counter;
void setup()
{
  pinMode(6, OUTPUT);
  Serial.begin(9600);
}
void loop()
{
  tamp = 0;
  for (counter = 0; counter < 10; ++counter) {
    if (tamp == 1) {
      tamp = 0;
      digitalWrite(6, HIGH);
    } else {
      tamp = 1;
      digitalWrite(6, LOW);
    }
    Serial.println(tamp);
    delay(1000);
  }
}